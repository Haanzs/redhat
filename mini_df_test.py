import unittest
from mini_df import bytes_to_MB

class TestMiniDf(unittest.TestCase):
        def test_byte_to_MB(self):
                # Test when argumnet in bytes is 0
                self.assertEqual(bytes_to_MB(0), 0)
                # Test when argumnet in bytes is 100
                self.assertEqual(bytes_to_MB(100000), 0.1)
                # Test when argumnet in bytes is 233 (rounded in function)
                self.assertEqual(bytes_to_MB(23300), 0.023)
 
if __name__ == '__main__':
    unittest.main()
