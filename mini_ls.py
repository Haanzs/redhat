import sys
import getopt
import os
from pwd import getpwuid
import time

oct_number_to_permisson_key = 	{0:"---",
								 1:"--x",		
								 2:"-w-",
								 3:"-wx",
								 4:"r--",
								 5:"r-x",
								 6:"rw-",
								 7:"rwx"}

def find_info(filename):
	try:
		owner = getpwuid(os.stat(filename).st_uid).pw_name
	except FileNotFoundError:
		print(f"File not found error for {filename}")
		sys.exit() 
	last_edit = time.ctime(os.path.getmtime(filename))
	permission = oct(os.stat(filename).st_mode)
	return owner, last_edit, permission
	
def octal_to_permission_symbol(octal_number):
	out_string = ""
	for num in octal_number:
		out_string = out_string + oct_number_to_permisson_key[int(num)]
	return out_string
	
def main(argv):
	opts=[]
	
	try:
		options, arguments = getopt.getopt(argv,"-r")
	except getopt.GetoptError:
		print('Unknown option argument')
		sys.exit()
	for opt,arg in options:
		opts.append(opt)
	
	if len(arguments) == 0:
		arguments.append(os.getcwd())
	
	for argument in arguments:	
		InfoAboutFile = find_info(argument)
		print(f"For file/directory: {argument} Owner:{InfoAboutFile[0]}, Last modified time:{InfoAboutFile[1]}, Access permission:{octal_to_permission_symbol(InfoAboutFile[2][-3:])}")
			
		if "-r" in opts:
			if os.path.isdir(argument):
				list_dir = os.listdir(argument)
				for element in list_dir:
					current_path = argument + "/"
					path = (os.path.join(current_path, element))
					if os.path.isdir(path):
						main(['-r', path])
				
if __name__ == "__main__":
   main(sys.argv[1:])




